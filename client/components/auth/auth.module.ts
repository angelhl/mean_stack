'use strict';

angular.module('pruebaApp.auth', [
  'pruebaApp.constants',
  'pruebaApp.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
