'use strict';

(function(){

class MyrouteComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('pruebaApp')
  .component('myroute', {
    templateUrl: 'app/myroute/myroute.html',
    controller: MyrouteComponent,
    controllerAs: 'myrouteCtrl'
  });

})();
