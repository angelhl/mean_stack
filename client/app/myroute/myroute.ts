'use strict';

angular.module('pruebaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/myroute', {
        template: '<myroute></myroute>'
      });
  });
