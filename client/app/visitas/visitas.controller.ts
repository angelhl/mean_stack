'use strict';

(function(){

class VisitasComponent {
  constructor() {
    this.message = 'Hello';
  }
}

angular.module('pruebaApp')
  .component('visitas', {
    templateUrl: 'app/visitas/visitas.html',
    controller: VisitasComponent,
    controllerAs: 'visitasCtrl'
  });

})();
