'use strict';

describe('Component: VisitasComponent', function () {

  // load the controller's module
  beforeEach(module('pruebaApp'));

  var VisitasComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController) {
    VisitasComponent = $componentController('visitas', {});
  }));

  it('should ...', function () {
    expect(1).to.equal(1);
  });
});
