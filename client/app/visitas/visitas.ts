'use strict';

angular.module('pruebaApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/visitas', {
        template: '<visitas></visitas>'
      });
  });
