'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var mensajeCtrlStub = {
  index: 'mensajeCtrl.index',
  show: 'mensajeCtrl.show',
  create: 'mensajeCtrl.create',
  update: 'mensajeCtrl.update',
  destroy: 'mensajeCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var mensajeIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './mensaje.controller': mensajeCtrlStub
});

describe('Mensaje API Router:', function() {

  it('should return an express router instance', function() {
    expect(mensajeIndex).to.equal(routerStub);
  });

  describe('GET /api/mensajes', function() {

    it('should route to mensaje.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'mensajeCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/mensajes/:id', function() {

    it('should route to mensaje.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'mensajeCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/mensajes', function() {

    it('should route to mensaje.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'mensajeCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/mensajes/:id', function() {

    it('should route to mensaje.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'mensajeCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/mensajes/:id', function() {

    it('should route to mensaje.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'mensajeCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/mensajes/:id', function() {

    it('should route to mensaje.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'mensajeCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
