'use strict';

var app = require('../..');
import request from 'supertest';

var newMensaje;

describe('Mensaje API:', function() {

  describe('GET /api/mensajes', function() {
    var mensajes;

    beforeEach(function(done) {
      request(app)
        .get('/api/mensajes')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          mensajes = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(mensajes).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/mensajes', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/mensajes')
        .send({
          name: 'New Mensaje',
          info: 'This is the brand new mensaje!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newMensaje = res.body;
          done();
        });
    });

    it('should respond with the newly created mensaje', function() {
      expect(newMensaje.name).to.equal('New Mensaje');
      expect(newMensaje.info).to.equal('This is the brand new mensaje!!!');
    });

  });

  describe('GET /api/mensajes/:id', function() {
    var mensaje;

    beforeEach(function(done) {
      request(app)
        .get('/api/mensajes/' + newMensaje._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          mensaje = res.body;
          done();
        });
    });

    afterEach(function() {
      mensaje = {};
    });

    it('should respond with the requested mensaje', function() {
      expect(mensaje.name).to.equal('New Mensaje');
      expect(mensaje.info).to.equal('This is the brand new mensaje!!!');
    });

  });

  describe('PUT /api/mensajes/:id', function() {
    var updatedMensaje;

    beforeEach(function(done) {
      request(app)
        .put('/api/mensajes/' + newMensaje._id)
        .send({
          name: 'Updated Mensaje',
          info: 'This is the updated mensaje!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedMensaje = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMensaje = {};
    });

    it('should respond with the updated mensaje', function() {
      expect(updatedMensaje.name).to.equal('Updated Mensaje');
      expect(updatedMensaje.info).to.equal('This is the updated mensaje!!!');
    });

  });

  describe('DELETE /api/mensajes/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/mensajes/' + newMensaje._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when mensaje does not exist', function(done) {
      request(app)
        .delete('/api/mensajes/' + newMensaje._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
