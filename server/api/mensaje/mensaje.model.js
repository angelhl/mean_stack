'use strict';

import mongoose from 'mongoose';

var MensajeSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Mensaje', MensajeSchema);
