/**
 * Mensaje model events
 */

'use strict';

import {EventEmitter} from 'events';
import Mensaje from './mensaje.model';
var MensajeEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MensajeEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Mensaje.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    MensajeEvents.emit(event + ':' + doc._id, doc);
    MensajeEvents.emit(event, doc);
  }
}

export default MensajeEvents;
