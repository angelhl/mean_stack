'use strict';

var app = require('../..');
import request from 'supertest';

var newVisitas;

describe('Visitas API:', function() {

  describe('GET /api/visitass', function() {
    var visitass;

    beforeEach(function(done) {
      request(app)
        .get('/api/visitass')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          visitass = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(visitass).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/visitass', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/visitass')
        .send({
          name: 'New Visitas',
          info: 'This is the brand new visitas!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newVisitas = res.body;
          done();
        });
    });

    it('should respond with the newly created visitas', function() {
      expect(newVisitas.name).to.equal('New Visitas');
      expect(newVisitas.info).to.equal('This is the brand new visitas!!!');
    });

  });

  describe('GET /api/visitass/:id', function() {
    var visitas;

    beforeEach(function(done) {
      request(app)
        .get('/api/visitass/' + newVisitas._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          visitas = res.body;
          done();
        });
    });

    afterEach(function() {
      visitas = {};
    });

    it('should respond with the requested visitas', function() {
      expect(visitas.name).to.equal('New Visitas');
      expect(visitas.info).to.equal('This is the brand new visitas!!!');
    });

  });

  describe('PUT /api/visitass/:id', function() {
    var updatedVisitas;

    beforeEach(function(done) {
      request(app)
        .put('/api/visitass/' + newVisitas._id)
        .send({
          name: 'Updated Visitas',
          info: 'This is the updated visitas!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedVisitas = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedVisitas = {};
    });

    it('should respond with the updated visitas', function() {
      expect(updatedVisitas.name).to.equal('Updated Visitas');
      expect(updatedVisitas.info).to.equal('This is the updated visitas!!!');
    });

  });

  describe('DELETE /api/visitass/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/visitass/' + newVisitas._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when visitas does not exist', function(done) {
      request(app)
        .delete('/api/visitass/' + newVisitas._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
