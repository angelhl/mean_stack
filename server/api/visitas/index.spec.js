'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var visitasCtrlStub = {
  index: 'visitasCtrl.index',
  show: 'visitasCtrl.show',
  create: 'visitasCtrl.create',
  update: 'visitasCtrl.update',
  destroy: 'visitasCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var visitasIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './visitas.controller': visitasCtrlStub
});

describe('Visitas API Router:', function() {

  it('should return an express router instance', function() {
    expect(visitasIndex).to.equal(routerStub);
  });

  describe('GET /api/visitass', function() {

    it('should route to visitas.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'visitasCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/visitass/:id', function() {

    it('should route to visitas.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'visitasCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/visitass', function() {

    it('should route to visitas.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'visitasCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/visitass/:id', function() {

    it('should route to visitas.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'visitasCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/visitass/:id', function() {

    it('should route to visitas.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'visitasCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/visitass/:id', function() {

    it('should route to visitas.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'visitasCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
