/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/visitass              ->  index
 * POST    /api/visitass              ->  create
 * GET     /api/visitass/:id          ->  show
 * PUT     /api/visitass/:id          ->  update
 * DELETE  /api/visitass/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Visitas from './visitas.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Visitass
export function index(req, res) {
  return Visitas.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Visitas from the DB
export function show(req, res) {
  return Visitas.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Visitas in the DB
export function create(req, res) {
  return Visitas.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Visitas in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Visitas.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Visitas from the DB
export function destroy(req, res) {
  return Visitas.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
