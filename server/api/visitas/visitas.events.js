/**
 * Visitas model events
 */

'use strict';

import {EventEmitter} from 'events';
import Visitas from './visitas.model';
var VisitasEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
VisitasEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Visitas.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    VisitasEvents.emit(event + ':' + doc._id, doc);
    VisitasEvents.emit(event, doc);
  }
}

export default VisitasEvents;
