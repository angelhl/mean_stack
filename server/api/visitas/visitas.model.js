'use strict';

import mongoose from 'mongoose';

var VisitasSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean,
  direccion: String
});

export default mongoose.model('Visitas', VisitasSchema);
